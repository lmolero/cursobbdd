﻿SELECT DISTINCT
  *
FROM equipo
  JOIN ciclista
    ON equipo.nomequipo = ciclista.nomequipo
  JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
  JOIN puerto
    ON etapa.numetapa = puerto.numetapa;

--c1
SELECT DISTINCT numetapa,
       dorsal
  FROM etapa
  JOIN puerto ON etapa.numetapa = puerto.numetapa;

-- c2
  SELECT * FROM ciclista  
  JOIN ( SELECT etapa.numetapa,
       etapa.dorsal
  FROM etapa
  JOIN puerto ON etapa.numetapa = puerto.numetapa) AS c1
  ON c1.dorsal=ciclista.dorsal; 

-- consulta final 
SELECT *
  FROM equipo
  JOIN (  SELECT DISTINCT * FROM ciclista  
  JOIN ( SELECT etapa.numetapa,
       etapa.dorsal
  FROM etapa
  JOIN puerto ON etapa.numetapa = puerto.numetapa) AS c1
  ON c1.dorsal=ciclista.dorsal
  ) AS c2
  ON c2.nomequipo=equipo.nomequipo;
 
-- creando vistas para facilitar las consultas 

CREATE OR REPLACE VIEW consulta1C1 AS 
SELECT DISTINCT etapa.numetapa,
       etapa.dorsal
  FROM etapa
  JOIN puerto ON etapa.numetapa = puerto.numetapa;

SELECT * FROM consulta1C1;

CREATE OR REPLACE VIEW consulta1c2  AS 
SELECT * FROM ciclista  
  JOIN consulta1C1 C1 
  ON c1.dorsal=ciclista.dorsal;


SELECT * FROM  consulta1c2 ;

/* consulta de combinacion externa ejemplo*/
  SELECT DISTINCT  *
         FROM ciclista
    JOIN  puerto ON ciclista.dorsal = puerto.dorsal
    WHERE altura>1200;

  /* mejorar la consulta mediante las subconsultas*/
    -- ciclistas que han ganado etapa 
-- c1
SELECT DISTINCT dorsal FROM puerto  WHERE altura>1200;
-- c2
SELECT ciclista.* 
  FROM  (SELECT DISTINCT dorsal FROM puerto  WHERE altura>1200) AS c1 
  JOIN ciclista ON  c1.dorsal=ciclista.dorsal;

CREATE OR REPLACE VIEW consulta2c1 AS
SELECT DISTINCT  ciclista.*
         FROM ciclista
    JOIN  puerto ON ciclista.dorsal = puerto.dorsal
    WHERE altura>1200;

CREATE OR REPLACE VIEW consulta2c2 AS
SELECT ciclista.* 
  FROM consulta2c1 AS c1
  JOIN ciclista USING (dorsal);

-- ciclistas que NO han ganado etapa 
 -- c1

SELECT DISTINCT ciclista.dorsal,
       nombre
  FROM ciclista
  LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal
  WHERE etapa.dorsal IS NULL;
   


