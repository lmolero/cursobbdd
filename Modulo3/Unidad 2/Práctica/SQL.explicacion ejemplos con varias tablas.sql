﻿
/*PARA JUNTAR 2 TABLAS*/ 

SELECT * FROM ciclista INNER JOIN etapa ON   ciclista.dorsal=etapa.dorsal;

SELECT * FROM ciclista, etapa WHERE  ciclista.dorsal=etapa.dorsal;

SELECT * FROM ciclista INNER JOIN etapa USING (dorsal);

/* combinacion puerto, ciclista*/

SELECT * FROM ciclista NATURAL JOIN puerto;

SELECT * FROM ciclista  INNER JOIN puerto ON  ciclista.dorsal=puerto.dorsal;

SELECT * FROM ciclista, puerto WHERE  ciclista.dorsal=puerto.dorsal;

SELECT * FROM ciclista INNER JOIN puerto USING (dorsal);

/* PARA JUNTAR 3 TABLAS*/
  SELECT * 
  FROM equipo JOIN ciclista JOIN etapa
  ON 
  ciclista.nomequipo=equipo.nomequipo AND ciclista.dorsal=etapa.dorsal;

  SELECT *
    FROM  equipo JOIN   ciclista 
    ON   
    equipo.nomequipo=ciclista.nomequipo
    JOIN etapa ON ciclista.dorsal=etapa.dorsal;

  SELECT * FROM  equipo
    JOIN ciclista USING(nomequipo)
    JOIN etapa USING(dorsal);

  SELECT DISTINCT dorsal FROM lleva INNER JOIN maillot ON  lleva.código=maillot.código;

