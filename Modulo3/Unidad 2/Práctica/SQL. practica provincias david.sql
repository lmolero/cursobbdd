﻿SELECT provincia FROM provincias;
SELECT provincia,poblacion/superficie densidad FROM provincias;
SELECT sqrt (2);



SELECT provincia,CHAR_LENGTH(provincia) FROM provincias;

  SELECT DISTINCT autonomia FROM provincias;

SELECT provincia FROM provincias
WHERE provincia=autonomia;

SELECT provincia FROM provincias  WHERE provincia LIKE "%ue%";

SELECT provincia FROM provincias WHERE provincia LIKE "a%" OR "á%";

SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE "%ana";

SELECT DISTINCT autonomia,CHAR_LENGTH(autonomia)  FROM provincias
  ORDER BY autonomia DESC;

SELECT DISTINCT provincia FROM provincias WHERE autonomia LIKE '% %';

SELECT provincia FROM provincias WHERE provincia LIKE '% %';

SELECT DISTINCT autonomia FROM provincias WHERE autonomia NOT LIKE '% %' ORDER BY autonomia DESC ;

SELECT DISTINCT autonomia FROM provincias WHERE provincia LIKE '% %'  ORDER BY autonomia ASC;

SELECT provincia FROM provincias WHERE provincia NOT LIKE '% %';

SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE '% %' ORDER BY autonomia DESC;

SELECT DISTINCT autonomia FROM provincias WHERE  autonomia LIKE 'can%'ORDER BY autonomia ASC;

SELECT DISTINCT autonomia FROM provincias WHERE poblacion >=1000000 ORDER BY autonomia ASC;

SELECT SUM(poblacion) FROM provincias;

SELECT COUNT(provincia) FROM  provincias;

SELECT autonomia FROM provincia ;

SELECT LOCATE('la','lalalala');

SELECT autonomia FROM provincias WHERE LOCATE(provincia,autonomia)>0;


SELECT provincia FROM provincias WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
  UNION
SELECT autonomia FROM provincias WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;

SELECT  SUM(superficie) FROM provincias;

SELECT provincia FROM provincias ORDER BY provincia ASC  LIMIT 1 ;

SELECT provincia FROM provincias WHERE CHAR_LENGTH(provincia)>CHAR_LENGTH(autonomia);

SELECT COUNT(DISTINCT autonomia) FROM provincias ;   

SELECT MIN(CHAR_LENGTH(autonomia)) FROM provincias ;

SELECT MAX(CHAR_LENGTH(provincia)) FROM provincias ;

SELECT round (AVG(poblacion))
  FROM provincias 
WHERE poblacion BETWEEN 2000000 AND 3000000;

SELECT DISTINCT autonomia FROM provincias WHERE 
lower (provincia) LIKE '%á%' COLLATE utf8_bin OR
lower (provincia) LIKE '%é%' COLLATE utf8_bin OR 
lower (provincia) LIKE '%í%' COLLATE utf8_bin OR
lower (provincia) LIKE '%ó%' COLLATE utf8_bin OR
lower (provincia) LIKE '%ú%' COLLATE utf8_bin;

SELECT MAX(poblacion) FROM provincias ; 

SELECT provincia FROM provincias WHERE poblacion=(
SELECT MAX(poblacion) FROM provincias 
);

SELECT MAX(poblacion) FROM provincias WHERE poblacion<=1000000;

SELECT provincia FROM provincias WHERE poblacion=(
SELECT MAX(poblacion) FROM provincias WHERE poblacion<=1000000
  );

SELECT provincia FROM provincias WHERE poblacion=(
SELECT MIN(poblacion) FROM provincias WHERE poblacion>=1000000
  );

SELECT autonomia FROM provincias WHERE superficie=(
SELECT MAX(superficie) FROM provincias 
);

SELECT provincia FROM provincias WHERE poblacion>(
SELECT AVG(poblacion) FROM provincias 
);

SELECT  DENSE_RANK(poblacion) FROM provincias; 

/*(37) Densidad de población del país*/

SELECT SUM(p.poblacion) 
FROM provincias p;

SELECT SUM(p.superficie) 
FROM provincias p;

    
SELECT (SELECT SUM(p.poblacion) 
FROM provincias p)/(SELECT SUM(p.superficie) 
FROM provincias p);c 

/*(38) ¿Cuántas provincias tiene cada comunidad autónoma?*/

SELECT p.autonomia, COUNT(p.provincia)
        FROM provincias p
  GROUP BY p.autonomia;

/*(39) Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir*/

SELECT COUNT(p.provincia),p.autonomia
        FROM provincias p
  GROUP BY p.autonomia  ORDER BY COUNT(p.provincia)  DESC , p.autonomia asc;

/*(40) ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?*/
  
  SELECT DISTINCT autonomia,COUNT(*) 
  FROM provincias 
  WHERE provincia LIKE '% %' GROUP BY autonomia;

/*(41) Autonomías uniprovinciales*/
  


  -- (17) Mostrar los datos de los empleados cuyo oficio sea ANALISTA
  SELECT * FROM emple e
    WHERE e.oficio='analista';

  -- (18) Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000
     SELECT * FROM emple e
    WHERE e.oficio='analista'AND e.salario>2000;

-- (33) Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre

  SELECT e.apellido FROM emple e
    ORDER BY e.oficio,e.apellido;

-- (36) Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados
  SELECT * FROM emple e
    WHERE e.apellido NOT LIKE'%z' ;

-- (14) Mostrar el código de los empleados cuyo salario sea mayor que 2000
  SELECT e.emp_no
    FROM emple e
    WHERE e.salario>2000;

 -- (15) Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
  SELECT e.emp_no, e.apellido
    FROM emple e
    WHERE e.salario<2000;

-- (16) Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500
 SELECT *
    FROM emple e
    WHERE e.salario BETWEEN 1500 AND 2500;

-- (19) Seleccionar el apellido y oficio de los empleados del departamento número 20
   SELECT e.apellido,
          e.oficio
    FROM emple e
    WHERE e.dept_no='20';
-- (21) Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.
 SELECT *
    FROM emple e
    WHERE e.apellido LIKE 'm%' OR e.apellido LIKE 'n%' ORDER BY e.apellido;

-- (22) Seleccionar los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.
  SELECT * FROM emple e
    WHERE e.oficio='vendedor' ORDER BY e.apellido;

-- (24) Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente
  SELECT * FROM emple e
    WHERE e.dept_no='10' AND e.oficio='analista' ORDER BY e.apellido, e.oficio;

-- (25) Realizar un listado de los distintos meses en que los empleados se han dado de alta
SELECT DISTINCT MONTH(e.fecha_alt)
  FROM emple e;

-- (26) Realizar un listado de los distintos años en los que los empleados se han dado de alta
  SELECT DISTINCT YEAR(e.fecha_alt)
  FROM emple e;

-- (27) Realizar un listado de los distintos días del mes en que los empleados se han dado de alta

 SELECT DISTINCT DAY(e.fecha_alt)
  FROM emple e;

-- (28) Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20
  SELECT e.apellido FROM emple e
    WHERE e.salario>2000 OR e.dept_no=20;

 -- (35) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados
   SELECT apellido FROM emple 
    WHERE apellido LIKE 'a%' OR apellido LIKE 'm%';

-- (37) Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición. Ordenar la salida por oficio y por salario de forma descendente
  SELECT * FROM emple 
    WHERE apellido LIKE 'a%' AND oficio LIKE '%e%'
  ORDER BY oficio,salario DESC;

 -- (34) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados
 SELECT apellido FROM emple 
    WHERE apellido LIKE 'a%';

-- (6) Indicar el número de empleados que hay
SELECT COUNT(*) FROM emple e;

-- (9) Indicar el número de departamentos que hay
  SELECT COUNT(*) FROM depart d;

-- (20) Contar el número de empleados cuyo oficio sea VENDEDOR

SELECT COUNT(*) FROM emple e
 WHERE e.oficio LIKE 'VENDEDOR';

-- (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece
  SELECT e.apellido,
       d.dnombre FROM emple e
  JOIN depart d ON e.dept_no = d.dept_no;
  
-- (30) Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.
 SELECT 
        e.apellido,
        e.oficio,
        d.dnombre
  FROM emple e
  JOIN depart d ON e.dept_no = d.dept_no ORDER BY e.apellido DESC;

-- (23) Mostrar los apellidos del empleado que más gana
  SELECT MAX(e.salario)  FROM emple e;

  SELECT e.apellido FROM emple e
    WHERE e.salario=(SELECT MAX(e.salario)  FROM emple e);

-- (10) Indicar el número de empleados más el número de departamentos

/*
SELECT ((SELECT COUNT( *) FROM emple e)+
 ( SELECT  COUNT(*) FROM depart d));
  */

 -- (31) Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan.

SELECT (e.emp_no) ,
       (e.dept_no) FROM  depart d
  JOIN emple e USING(dept_no);

SELECT dept_no, COUNT(*) FROM (SELECT (e.emp_no) ,
       (e.dept_no) FROM  depart d
  JOIN emple e USING(dept_no)) c1 GROUP BY dept_no ;

SELECT e.dept_no ,COUNT(*) FROM emple e
  GROUP BY e.dept_no;

 (32) Listar el número de empleados por departamento, ordenados de más a menos empleados, incluyendo los departamentos que no tengan empleados. El encabezado de la tabla será dnombre y NUMERO_DE_EMPLEADOS

SELECT (e.emp_no) ,
       (e.dept_no) FROM  depart d
  JOIN emple e USING(dept_no);

SELECT  c1.dnombre, COUNT(*) FROM (SELECT *
        FROM  depart d
  JOIN emple e USING(dept_no)) c1 GROUP BY dept_no ;

SELECT * 
  FROM
  (
    
    SELECT  c1.dnombre, COUNT(*)
    FROM (

      SELECT * FROM  depart d
        JOIN emple e USING(dept_no)
      ) c1 GROUP BY dept_no
  
  ) c1 LEFT JOIN emple e USING (dept_no)
WHERE e.dept_no IS NULL;
  
-- lo que funciona 
SELECT dept_no,COUNT(*) n FROM emple GROUP BY dept_no;

SELECT dnombre,IFNULL(n,0) n FROM (
    SELECT dept_no,COUNT(*) n FROM emple GROUP BY dept_no
  ) c1 RIGHT JOIN depart USING(dept_no);

-- (15) Listar el nombre de la persona y el nombre de su supervisor
SELECT  s.persona, s.supervisor
       FROM supervisa s;

-- (19) Indicar el nombre de las personas que trabajan para FAGOR

  SELECT t.persona
    FROM trabaja t
  WHERE t.compañia='fagor';

-- (22) Indicar el nombre de las personas que trabajan para FAGOR o para INDRA
    SELECT t.persona
    FROM trabaja t
  WHERE t.compañia='fagor' OR t.compañia='indra';

-- (01) Indicar el número de ciudades que hay en la tabla ciudades
  SELECT COUNT(*) FROM ciudad c;

-- (11) Listar el nombre de las personas y el número de habitantes de la ciudad donde viven
SELECT 

