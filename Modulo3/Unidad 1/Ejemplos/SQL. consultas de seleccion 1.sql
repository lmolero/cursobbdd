﻿/*consulta de selección 1*/ 
  SELECT DISTINCT  edad
  FROM ciclista;  

/*consulta de selección 2*/ 
  SELECT edad
  FROM ciclista
  WHERE nomequipo= 'Artiach' ;

/*consulta de selección 3*/ 
SELECT DISTINCT edad 
FROM ciclista 
WHERE nomequipo= 'Artiach' OR nomequipo='Amore Vita' ;

/*consulta de selección 4*/ 
SELECT dorsal 
FROM ciclista 
WHERE edad<25 OR edad>30;

/*consulta de selección 5*/ 
SELECT dorsal 
FROM ciclista 
WHERE  edad BETWEEN 28 AND 32 AND nomequipo='Banesto';

/*consulta de selección 6*/
SELECT nombre 
FROM ciclista 
WHERE CHAR_LENGTH(nombre)>8;

/*consulta de selección 6*/
SELECT nombre 
FROM ciclista 
WHERE LENGTH(nombre)>8;

/*consulta de selección 7*/
SELECT dorsal,
       nombre,
      UPPER (nombre) nombre_mayúscula 
FROM ciclista;

/*consulta de selección 8*/
  SELECT DISTINCT lleva.dorsal 
  FROM lleva,maillot 
  WHERE maillot.color='amarillo';

/*consulta de selección 9*/
  SELECT nompuerto 
  FROM puerto 
  WHERE puerto.altura>1500;

/*consulta de selección 10*/
SELECT puerto.dorsal 
FROM puerto 
WHERE  pendiente>=8 OR altura BETWEEN 1800 AND 3000;

/*consulta de selección 11*/
SELECT puerto.dorsal 
FROM puerto WHERE pendiente>=8 AND altura BETWEEN 1800 AND 3000;

/*consulta de seleción 11 con interseccion* de sql NO  Mysql*/

/*  SELECT puerto.dorsal 
FROM puerto WHERE pendiente>=8 ;
INTERSECT
SELECT puerto.dorsal 
FROM puerto WHERE altura BETWEEN 1800 AND 3000;*/

  /*CONSULTAS DE SELECCION 2*/

  /* CONSULTA DE SELECION 2.1*/

SELECT COUNT(dorsal) 
FROM ciclista;

/* CONSULTA DE SELECION 2.2*/

SELECT COUNT(ciclista.dorsal) 
FROM ciclista 
WHERE nomequipo = 'banesto' ;


/* CONSULTA DE SELECION 2.3*/

SELECT ROUND(AVG(edad)) AS edadmedia
FROM Ciclista;


/* CONSULTA DE SELECION 2.4*/
SELECT AVG(ciclista.edad) 
FROM Ciclista
WHERE nomequipo= 'banesto';

/* CONSULTA DE SELECION 2.5*/
SELECT nomequipo, AVG (ciclista.edad) 
FROM  Ciclista GROUP BY (nomequipo); 

/* CONSULTA DE SELECION 2.6*/
  SELECT nomequipo, COUNT(dorsal)
  FROM  ciclista GROUP BY(nomequipo); 

/* CONSULTA DE SELECION 2.7*/
  SELECT COUNT(nompuerto)
  FROM puerto;

/* CONSULTA DE SELECION 2.8*/
SELECT COUNT(nompuerto)
  FROM puerto
  WHERE altura>=1500;

/* CONSULTA DE SELECION 2.9*/
  SELECT nomequipo 
    FROM ciclista
GROUP BY nomequipo
HAVING COUNT(dorsal) > 4;

/* CONSULTA DE SELECION 2.10*/
    SELECT nomequipo 
    FROM ciclista
      WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo
HAVING COUNT(dorsal) > 4;

/* CONSULTA DE SELECION 2.11*/
SELECT dorsal,COUNT(numetapa) 
 FROM etapa
GROUP BY dorsal;

/* CONSULTA DE SELECION 2.12*/

SELECT dorsal 
  FROM etapa
  GROUP BY dorsal 
  HAVING COUNT(*)>1



